import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    startingTotal: 20
  },
  mutations: {
    setStartingTotal(state, newTotal) {
      state.startingTotal = newTotal;
    }
  },
  actions: {}
});
