import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faHome, faCog, faEllipsisH } from '@fortawesome/free-solid-svg-icons';

Vue.config.productionTip = false;

library.add(faHome);
library.add(faCog);
library.add(faEllipsisH)
Vue.component('font-awesome-icon', FontAwesomeIcon);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
